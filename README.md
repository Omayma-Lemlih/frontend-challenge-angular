# Frontend Challenge Angular

- A small webapp that will list the most starred Github repos.

## Instructions

Clone project: `git clone https://Omayma-Lemlih@bitbucket.org/Omayma-Lemlih/frontend-challenge-angular.git`

Install node dependencies: `npm install`

Run project: `ng serve` for a dev server.
Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Resources

-[Moment.js](https://momentjs.com/)
-[ngx-infinite-scroll](https://www.npmjs.com/package/ngx-infinite-scroll)
