import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Result } from '../models/result.model';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GithubService {

  private readonly BASE_URL = 'https://api.github.com/search/repositories?q=created:>2017-10-22&sort=stars&order=desc&page=';

  constructor(
    private http: HttpClient
  ) { }

  public getRepos(page: number): Promise<Result> {
    return this.http.get<Result>(this.BASE_URL + page).pipe(take(1)).toPromise();
  }

}
