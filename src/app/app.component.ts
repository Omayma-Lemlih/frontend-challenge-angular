import { Component, OnInit } from '@angular/core';
import { GithubService } from 'src/app/services/github.service';
import { Result, Item } from 'src/app/models/result.model';
import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'frontend-challenge-angular';

  private page = 1;
  private result: Result;
  public items: Item[] = [];

  constructor(
    private githubService: GithubService
  ) { }

  async ngOnInit() {
    try {
      this.result = await this.githubService.getRepos(this.page);
      console.log(this.result);
      this.items = this.result.items;
      this.items.forEach((item: Item) => {
        item.created_at = moment(item.created_at).fromNow();
      });
    } catch (error) {
      console.log(error);
    }
  }

  async onScroll() {
    try {
      this.page++;
      this.result = await this.githubService.getRepos(this.page);
      for (const item of this.result.items) {
        this.items.push(item);
      }
      console.log(this.items);
    } catch (error) {
      console.log(error);
    }
  }

}
